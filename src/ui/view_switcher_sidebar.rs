/*
 * This file is part of Deste.
 *
 * Deste is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * Deste is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Deste. If
 * not, see <https://www.gnu.org/licenses/>.
*/

use std::sync::OnceLock;

use gtk::gio;
use gtk::glib;
use gtk::CompositeTemplate;

use glib::subclass::Signal;

use gtk::prelude::*;
use gtk::subclass::prelude::*;

use crate::ui::ViewSwitcherRow;

mod imp {
    use super::*;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(resource = "/org/dysliked/deste/ui/view_switcher_sidebar.ui")]
    pub struct ViewSwitcherSidebarImpl {
        #[template_child]
        pub(super) listbox: TemplateChild<gtk::ListBox>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for ViewSwitcherSidebarImpl {
        const NAME: &'static str = "DesteViewSwitcherSidebar";
        type Type = super::ViewSwitcherSidebar;
        type ParentType = gtk::Widget;

        fn class_init(klass: &mut Self::Class) {
            Self::bind_template(klass);
            Self::bind_template_callbacks(klass);
        }

        fn instance_init(object: &glib::subclass::InitializingObject<Self>) {
            object.init_template();
        }
    }

    impl ObjectImpl for ViewSwitcherSidebarImpl {
        fn dispose(&self) {
            self.listbox.unparent();
        }

        fn signals() -> &'static [Signal] {
            static SIGNALS: OnceLock<Vec<Signal>> = OnceLock::new();

            SIGNALS.get_or_init(|| {
                vec![
                    Signal::builder("changed")
                        .param_types([glib::GString::static_type()])
                        .build(),
                ]
            })
        }
    }

    impl WidgetImpl for ViewSwitcherSidebarImpl {
    }

    #[gtk::template_callbacks]
    impl ViewSwitcherSidebarImpl {
        #[template_callback]
        fn on_changed(&self, row: Option<&gtk::ListBoxRow>) {
            if let Some(x) = &row {
                if let Some(child) = x.child() {
                    let name = child.downcast::<ViewSwitcherRow>().unwrap().name().clone();

                    self.obj().emit_by_name::<()>("changed", &[&name]);
                }
            }
        }
    }
}

glib::wrapper! {
    pub struct ViewSwitcherSidebar(ObjectSubclass<imp::ViewSwitcherSidebarImpl>)
        @extends gtk::Widget,
        @implements gtk::Buildable, gtk::ConstraintTarget, gtk::Native;
}

impl ViewSwitcherSidebar {
    pub fn new() -> Self {
        glib::Object::builder()
            .build()
    }

    pub fn set_model(&self, model: Option<&gio::ListModel>) {
        self.imp().listbox.bind_model(model, |item| {
            let widget = ViewSwitcherRow::default();

            item
                .bind_property("name", &widget, "name")
                .flags(glib::BindingFlags::DEFAULT | glib::BindingFlags::SYNC_CREATE)
                .build();

            item
                .bind_property("icon_name", &widget, "icon")
                .flags(glib::BindingFlags::DEFAULT | glib::BindingFlags::SYNC_CREATE)
                .build();

            item
                .bind_property("title", &widget, "title")
                .flags(glib::BindingFlags::DEFAULT | glib::BindingFlags::SYNC_CREATE)
                .build();

            widget.into()
        });
    }

    pub fn visible_page_name(&self) -> Option<glib::GString> {
        let imp = self.imp();

        match imp.listbox.selected_row() {
            None => None,
            Some(x) => Some(x.child().unwrap().downcast::<ViewSwitcherRow>().unwrap().name()),
        }
    }
}
