/*
 * This file is part of Deste.
 *
 * Deste is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * Deste is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Deste. If
 * not, see <https://www.gnu.org/licenses/>.
*/

use std::cell::RefCell;

use gtk::glib;
use gtk::CompositeTemplate;

use adw::prelude::*;
use adw::subclass::prelude::*;

mod imp {
    use super::*;

    #[derive(Debug, Default, CompositeTemplate, glib::Properties)]
    #[properties(wrapper_type = super::ViewSwitcherRow)]
    #[template(resource = "/org/dysliked/deste/ui/view_switcher_row.ui")]
    pub struct ViewSwitcherRowImpl {
        #[template_child]
        pub(super) image: TemplateChild<gtk::Image>,
        #[template_child]
        pub(super) label: TemplateChild<gtk::Label>,

        #[property(get, set)]
        pub(super) name: RefCell<glib::GString>,
        #[property(get, set = Self::set_title)]
        pub(super) title: RefCell<glib::GString>,
        #[property(get, set = Self::set_icon)]
        pub(super) icon: RefCell<Option<glib::GString>>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for ViewSwitcherRowImpl {
        const NAME: &'static str = "DesteViewSwitcherRow";
        type Type = super::ViewSwitcherRow;
        type ParentType = gtk::Widget;

        fn class_init(klass: &mut Self::Class) {
            Self::bind_template(klass);
        }

        fn instance_init(object: &glib::subclass::InitializingObject<Self>) {
            object.init_template();
        }
    }

    #[glib::derived_properties]
    impl ObjectImpl for ViewSwitcherRowImpl {
        fn dispose(&self) {
            self.image.unparent();
            self.label.unparent();
        }
    }

    impl WidgetImpl for ViewSwitcherRowImpl {
    }

    impl ViewSwitcherRowImpl {
        pub fn set_icon(&self, icon: Option<glib::GString>) {
            match &icon {
                None => self.image.set_icon_name(None),
                Some(x) => self.image.set_icon_name(Some(x.as_str())),
            }
            self.icon.replace(icon);
        }

        pub fn set_title(&self, title: glib::GString) {
            self.label.set_label(title.as_str());
            self.title.replace(title);
        }
    }
}

glib::wrapper! {
    pub struct ViewSwitcherRow(ObjectSubclass<imp::ViewSwitcherRowImpl>)
        @extends gtk::Widget,
        @implements gtk::Buildable, gtk::ConstraintTarget, gtk::Native;
}

impl Default for ViewSwitcherRow {
    fn default() -> Self {
        glib::Object::builder()
            .build()
    }
}
