/*
 * This file is part of Deste.
 *
 * Deste is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either
 *
 * version 3 of the License, or (at your option) any later version.
 *
 * Deste is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Deste. If
 * not, see <https://www.gnu.org/licenses/>.
*/

use gtk::glib;
use gtk::CompositeTemplate;

use adw::prelude::*;
use adw::subclass::prelude::*;

use adw::subclass::prelude::PreferencesDialogImpl as AdwPreferencesDialogImpl;

use crate::application::Application;
use crate::category::Category;
use crate::category::CategoryGroup;
use crate::source::NewSourceButton;
use crate::source::Source;
use crate::source::SourceRow;

mod imp {
    use super::*;

    #[derive(Debug,Default,CompositeTemplate)]
    #[template(resource = "/org/dysliked/deste/ui/preferences_dialog.ui")]
    pub struct PreferencesDialogImpl {
        #[template_child]
        pub(super) db_page: TemplateChild<adw::PreferencesPage>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for PreferencesDialogImpl {
        const NAME: &'static str = "DestePreferencesDialog";
        type Type = super::PreferencesDialog;
        type ParentType = adw::PreferencesDialog;

        fn class_init(klass: &mut Self::Class) {
            Self::bind_template(klass);
        }

        fn instance_init(object: &glib::subclass::InitializingObject<Self>) {
            object.init_template();
        }
    }

    impl ObjectImpl for PreferencesDialogImpl {
        fn constructed(&self) {
            self.parent_constructed();

            self.setup_database();
        }
    }

    impl WidgetImpl for PreferencesDialogImpl {
    }

    impl AdwDialogImpl for PreferencesDialogImpl {
    }

    impl AdwPreferencesDialogImpl for PreferencesDialogImpl {
    }

    impl PreferencesDialogImpl {
        fn setup_database(&self) {
            let app = Application::default();

            for val in &app.categories() {
                let category = val.unwrap().downcast::<Category>().unwrap();
                let group = CategoryGroup::new(&category, Some(NewSourceButton::new(&category)));

                self.db_page.add(&group);

                for s_val in &app.sources(&glib::GString::from(category.name())) {
                    let source = s_val.unwrap().downcast::<Source>().unwrap();
                    let row = SourceRow::new(source);

                    group.add(&row);
                }
            }
        }
    }
}

glib::wrapper! {
    pub struct PreferencesDialog(ObjectSubclass<imp::PreferencesDialogImpl>)
        @extends gtk::Widget, adw::Dialog, adw::PreferencesDialog,
        @implements gtk::Native;
}

impl PreferencesDialog {
    pub fn new() -> Self {
        glib::Object::builder()
            .build()
    }
}
