/*
 * This file is part of Deste.
 *
 * Deste is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * Deste is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Deste. If
 * not, see <https://www.gnu.org/licenses/>.
*/

use crate::models::DbMedia;
use crate::models::DbSource;
use crate::models::DbCategory;

use crate::category::Category;

use crate::media::Media;

use crate::source::Source;

impl From<DbCategory> for Category {
    fn from(dbc: DbCategory) -> Self {
        Self::new(dbc.name(), dbc.title(), dbc.icon())
    }
}

impl From<DbMedia> for Media {
    fn from(dbm: DbMedia) -> Self {
        Self::new(dbm.name(), dbm.cover_path())
    }
}

impl From<DbSource> for Source {
    fn from(dbs: DbSource) -> Self {
        Self::new(dbs.path(), dbs.title())
    }
}
