/*
 * This file is part of Deste.
 *
 * Deste is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * Deste is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Deste. If
 * not, see <https://www.gnu.org/licenses/>.
*/

use gtk::gio;
use gtk::glib;

use adw::prelude::*;
use adw::subclass::prelude::*;

use gio::subclass::prelude::ApplicationImpl as GApplicationImpl;
use gio::ListModel;
use gio::ListStore;

use crate::about;
use crate::category::Category;
use crate::media::Media;
use crate::models;
use crate::source::Source;
use crate::window::Window;

mod imp {
    use super::*;

    #[derive(Debug, Default)]
    pub struct ApplicationImpl {
    }

    #[glib::object_subclass]
    impl ObjectSubclass for ApplicationImpl {
        const NAME: &'static str = "DesteApplication";
        type Type = super::Application;
        type ParentType = adw::Application;
    }

    impl ObjectImpl for ApplicationImpl {
        fn constructed(&self) {
            let obj = self.obj();

            self.parent_constructed();

            // Setup accels
            obj.setup_accels();
        }
    }

    impl GApplicationImpl for ApplicationImpl {
        fn activate(&self) {
            let object = self.obj();
            let window = Window::new(&*object);

            window.set_categories(Some(&object.categories()));

            window.present();
        }

        fn startup(&self) {
            let obj = self.obj();

            self.parent_startup();

            // Setup actions
            obj.setup_actions();
        }
    }

    impl GtkApplicationImpl for ApplicationImpl {
    }

    impl AdwApplicationImpl for ApplicationImpl {
    }

    impl ApplicationImpl {
    }
}

glib::wrapper! {
    pub struct Application(ObjectSubclass<imp::ApplicationImpl>)
        @extends gio::Application, gtk::Application, adw::Application,
        @implements gio::ActionGroup, gio::ActionMap;
}

impl Default for Application {
    fn default() -> Self {
        gio::Application::default()
            .and_downcast::<Self>()
            .unwrap()
    }
}

impl Application {
    pub fn new() -> Self {
        glib::Object::builder()
            .property("application-id", "org.gnome.Deste")
            .property("flags", gio::ApplicationFlags::FLAGS_NONE)
            .build()
    }

    pub fn add_source(&self, category: &Category, source: Source) {
        models::add_source(&category.name(), source.title(), source.path());
    }

    pub fn categories(&self) -> ListModel {
        let list = ListStore::new::<Category>();

        for db_category in models::get_all_categories() {
            list.append(&Category::from(db_category));
        }

        list.upcast::<ListModel>()
    }

    pub fn medias(&self, category: &glib::GString) -> ListModel {
        let list = ListStore::new::<Media>();

        for db_media in models::get_medias(&category.to_string()) {
            list.append(&Media::from(db_media));
        }

        list.upcast::<ListModel>()
    }

    pub fn remove_source(&self, source: Source) {
        models::remove_source(&source.title());
    }

    pub fn sources(&self, category: &glib::GString) -> ListModel {
        let list = ListStore::new::<Source>();

        for db_source in models::get_sources(&category.to_string()) {
            list.append(&Source::from(db_source));
        }

        list.upcast::<ListModel>()
    }

    pub fn setup_accels(&self) {
        self.set_accels_for_action("app.quit", &["<Ctrl>Q"]);

        self.set_accels_for_action("win.show-preferences", &["<Ctrl>comma"]);
        self.set_accels_for_action("win.show-help-overlay", &["<Ctrl>question"]);
    }

    pub fn setup_actions(&self) {
        let actions = [
            gio::ActionEntryBuilder::new("about")
                .activate(|app: &Self, _, _| {
                    if let Some(window) = app.active_window() {
                        about::show_window(&window);
                    }
                })
                .build(),
            gio::ActionEntryBuilder::new("quit")
                .activate(|app: &Self, _, _| app.quit())
                .build(),
        ];

        self.add_action_entries(actions);
    }
}
