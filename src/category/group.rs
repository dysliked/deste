/*
 * This file is part of Deste.
 *
 * Deste is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either
 *
 * version 3 of the License, or (at your option) any later version.
 *
 * Deste is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Deste. If
 * not, see <https://www.gnu.org/licenses/>.
*/

use std::cell::RefCell;

use gtk::glib;

use adw::prelude::*;
use adw::subclass::prelude::*;

use crate::category::Category;

mod imp {
    use super::*;

    #[derive(Debug, Default, glib::Properties)]
    #[properties(wrapper_type = super::CategoryGroup)]
    pub struct CategoryGroupImpl {
        #[property(get, set = Self::set_category, construct_only)]
        pub(super) category: RefCell<Option<Category>>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for CategoryGroupImpl {
        const NAME: &'static str = "DesteCategoryGroup";
        type Type = super::CategoryGroup;
        type ParentType = adw::PreferencesGroup;
    }

    #[glib::derived_properties]
    impl ObjectImpl for CategoryGroupImpl {
    }

    impl WidgetImpl for CategoryGroupImpl {
    }

    impl PreferencesGroupImpl for CategoryGroupImpl {
    }

    impl CategoryGroupImpl {
        pub fn set_category(&self, category: Option<Category>) {
            let obj = self.obj();

            match &category {
                None => obj.set_title(""),
                Some(category_obj) => obj.set_title(&category_obj.title()),
            }

            self.category.replace(category);
        }
    }
}

glib::wrapper! {
    pub struct CategoryGroup(ObjectSubclass<imp::CategoryGroupImpl>)
        @extends gtk::Widget, adw::PreferencesGroup,
        @implements gtk::Native;
}

impl CategoryGroup {
    pub fn new(category: &Category, suffix: Option<impl IsA<gtk::Widget>>) -> Self {
        glib::Object::builder()
            .property("category", category)
            .property("header-suffix", &suffix)
            .build()
    }
}

