/*
 * This file is part of Deste.
 *
 * Deste is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * Deste is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Deste. If
 * not, see <https://www.gnu.org/licenses/>.
*/

use gtk::gio;
use gtk::glib;
use gtk::CompositeTemplate;
use gtk::prelude::*;

use adw::subclass::prelude::*;

use crate::media::MediaWidget;

mod imp {
    use super::*;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(resource = "/org/dysliked/deste/ui/media_page.ui")]
    pub struct MediaPageImpl {
        #[template_child]
        pub(super) view: TemplateChild<gtk::GridView>,
        #[template_child]
        pub(super) window: TemplateChild<gtk::ScrolledWindow>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for MediaPageImpl {
        const NAME: &'static str = "DesteMediaPage";
        type Type = super::MediaPage;
        type ParentType = gtk::Widget;

        fn class_init(klass: &mut Self::Class) {
            Self::bind_template(klass);
        }

        fn instance_init(object: &glib::subclass::InitializingObject<Self>) {
            object.init_template();
        }
    }

    impl ObjectImpl for MediaPageImpl {
        fn constructed(&self) {
            self.parent_constructed();

            let factory = gtk::SignalListItemFactory::new();

            factory.connect_setup(|_, item| {
                let list_item = item.downcast_ref::<gtk::ListItem>().unwrap();
                let widget = MediaWidget::default();

                list_item.set_child(Some(&widget));

                list_item
                    .bind_property("item", &widget, "media")
                    .flags(glib::BindingFlags::DEFAULT | glib::BindingFlags::SYNC_CREATE)
                    .build();
            });

            self.view.set_factory(Some(&factory));
        }

        fn dispose(&self) {
            self.window.unparent();
        }
    }

    impl WidgetImpl for MediaPageImpl {
    }
}

glib::wrapper! {
    pub struct MediaPage(ObjectSubclass<imp::MediaPageImpl>)
        @extends gtk::Widget,
        @implements gtk::Native;
}

impl MediaPage {
    pub fn new() -> Self {
        glib::Object::builder()
            .build()
    }

    pub fn update(&self, model: gio::ListModel) {
        let imp = self.imp();
        let selection = gtk::SingleSelection::new(Some(model));

        selection.set_selected(gtk::INVALID_LIST_POSITION);

        imp.view.set_model(Some(&selection));
    }
}
