/*
 * This file is part of Deste.
 *
 * Deste is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * Deste is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Deste. If
 * not, see <https://www.gnu.org/licenses/>.
*/

use std::cell::RefCell;

use gtk::glib;
use gtk::CompositeTemplate;
use gtk::prelude::*;
use gtk::subclass::prelude::*;

use crate::media::Media;

mod imp {
    use super::*;

    #[derive(Debug, Default, CompositeTemplate, glib::Properties)]
    #[properties(wrapper_type = super::MediaWidget)]
    #[template(resource = "/org/dysliked/deste/ui/media_widget.ui")]
    pub struct MediaWidgetImpl {
        #[template_child]
        pub(super) frame: TemplateChild<gtk::AspectFrame>,
        #[template_child]
        pub(super) picture: TemplateChild<gtk::Picture>,

        #[property(get, set = Self::set_media)]
        pub(super) media: RefCell<Option<Media>>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for MediaWidgetImpl {
        const NAME: &'static str = "DesteMediaWidget";
        type Type = super::MediaWidget;
        type ParentType = gtk::Widget;

        fn class_init(klass: &mut Self::Class) {
            Self::bind_template(klass);
        }

        fn instance_init(object: &glib::subclass::InitializingObject<Self>) {
            object.init_template();
        }
    }

    #[glib::derived_properties]
    impl ObjectImpl for MediaWidgetImpl {
        fn dispose(&self) {
            self.frame.unparent();
        }
    }

    impl WidgetImpl for MediaWidgetImpl {
    }

    impl MediaWidgetImpl {
        pub fn set_media(&self, media: Option<Media>) {
            self.picture.set_filename(match &media {
                Some(x) => Some(x.cover_path()),
                None => None,
            });

            self.media.replace(media);
        }
    }
}

glib::wrapper! {
    pub struct MediaWidget(ObjectSubclass<imp::MediaWidgetImpl>)
        @extends gtk::Widget,
        @implements gtk::Buildable, gtk::ConstraintTarget, gtk::Native;
}

impl Default for MediaWidget {
    fn default() -> Self {
        glib::Object::builder()
            .build()
    }
}

impl MediaWidget {
    pub fn new(media: &Media) -> Self {
        glib::Object::builder()
            .property("media", media)
            .build()
    }
}
