/*
 * This file is part of Deste.
 *
 * Deste is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * Deste is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Deste. If
 * not, see <https://www.gnu.org/licenses/>.
*/

use gtk::glib;
use gtk::gdk;
use gtk::prelude::*;

use gtk::Window;

pub fn show_window(main_window: &Window) {
    let window = adw::AboutWindow::builder()
        .transient_for(main_window)
        .application_name("Deste")
        .application_icon("video-display")
        .version(env!("CARGO_PKG_VERSION"))
        .developer_name("dysliked")
        .developers([
            "Thibault Peypelut <thibault.pey@gmail.com>",
        ])
        .translator_credits("translator-credits")
        .copyright("Copyright © 2023–2024 Thibault Peypelut")
        .license_type(gtk::License::Gpl30)
        .build();

    let controller = gtk::EventControllerKey::new();

    controller.connect_key_pressed(glib::clone!(
        #[weak] window, #[upgrade_or] glib::Propagation::Stop, move |_, key, _, modifier| {
            if key == gdk::Key::w && modifier == gdk::ModifierType::CONTROL_MASK{
                window.close();
            }
            glib::Propagation::Proceed
        }
    ));

    window.add_controller(controller);

    window.present();
}
