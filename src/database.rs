/*
 * This file is part of Deste.
 *
 * Deste is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * Deste is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Deste. If
 * not, see <https://www.gnu.org/licenses/>.
*/

use std::fs;
use std::path::PathBuf;

use diesel::prelude::*;
use diesel::r2d2::ConnectionManager;
use diesel::r2d2::Pool;

use gtk::glib;
use once_cell::sync::Lazy;

type PoolType = Pool<ConnectionManager<SqliteConnection>>;

static DATABASE_DIR: Lazy<PathBuf> = Lazy::new(|| glib::user_cache_dir().join("deste"));
static POOL: Lazy<PoolType> = Lazy::new(|| init_pool());

fn init_pool() -> PoolType {
    fs::create_dir_all(DATABASE_DIR.as_path()).expect("Could not create the cache directory");

    let path = DATABASE_DIR.join("media_library.db");
    let manager = ConnectionManager::<SqliteConnection>::new(path.as_path().to_str().unwrap());

    Pool::builder().build(manager).expect("Could not build connection pool")
}

pub fn get_pool() -> PoolType {
    POOL.clone()
}
