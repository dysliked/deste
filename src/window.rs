/*
 * This file is part of Deste.
 *
 * Deste is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * Deste is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Deste. If
 * not, see <https://www.gnu.org/licenses/>.
*/

use gtk::gio;
use gtk::glib;
use gtk::CompositeTemplate;

use adw::prelude::*;
use adw::subclass::prelude::*;

use gtk::subclass::prelude::WindowImpl as GtkWindowImpl;

use crate::application::Application as DesteApplication;
use crate::media::MediaPage;
use crate::preferences_dialog::PreferencesDialog;
use crate::ui::ViewSwitcherSidebar;

mod imp {
    use super::*;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(resource = "/org/dysliked/deste/ui/window.ui")]
    pub struct WindowImpl {
        #[template_child]
        pub(super) split_view: TemplateChild<adw::NavigationSplitView>,
        #[template_child]
        pub(super) switcher: TemplateChild<ViewSwitcherSidebar>,
        #[template_child]
        pub(super) page: TemplateChild<MediaPage>,
        #[template_child]
        pub(super) content: TemplateChild<adw::NavigationPage>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for WindowImpl {
        const NAME: &'static str = "DesteWindow";
        type Type = super::Window;
        type ParentType = adw::ApplicationWindow;

        fn class_init(klass: &mut Self::Class) {
            Self::bind_template(klass);
            Self::bind_template_callbacks(klass);

            klass.install_action("win.show-help-overlay", None, move |win, _, _| {
            });

            klass.install_action("win.show-preferences", None, move |win, _, _| {
                PreferencesDialog::new().present(Some(win));
            });
        }

        fn instance_init(object: &glib::subclass::InitializingObject<Self>) {
            object.init_template();
        }
    }

    impl ObjectImpl for WindowImpl {
    }

    impl WidgetImpl for WindowImpl {
    }

    impl GtkWindowImpl for WindowImpl {
    }

    impl ApplicationWindowImpl for WindowImpl {
    }

    impl AdwApplicationWindowImpl for WindowImpl {
    }

    #[gtk::template_callbacks]
    impl WindowImpl {
        #[template_callback]
        fn change_page(&self, category_name: glib::GString) {
            self.obj().show_medias(&category_name);

            self.split_view.set_show_content(true);
        }
    }
}

glib::wrapper! {
    pub struct Window(ObjectSubclass<imp::WindowImpl>)
        @extends gtk::Widget, gtk::Window, gtk::ApplicationWindow, adw::ApplicationWindow,
        @implements gio::ActionGroup, gio::ActionMap, gtk::Native;
}

impl Window {
    pub fn new<A: IsA<gtk::Application>>(application: &A) -> Self {
        glib::Object::builder()
            .property("application", application)
            .build()
    }

    pub fn set_categories(&self, model: Option<&gio::ListModel>) {
        self.imp().switcher.set_model(model);
    }

    pub fn show_medias(&self, category: &glib::GString) {
        let imp = self.imp();
        let app = DesteApplication::default();
        let model = app.medias(category);

        imp.page.update(model.clone());
    }
}

