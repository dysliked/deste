/*
 * This file is part of Deste.
 *
 * Deste is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * Deste is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Deste. If
 * not, see <https://www.gnu.org/licenses/>.
*/

mod about;
mod application;
mod category;
mod convert;
mod database;
mod media;
mod models;
mod preferences_dialog;
mod schema;
mod source;
mod ui;
mod window;

use gtk::gio;
use gtk::glib;
use gtk::prelude::ApplicationExtManual;

use application::Application;

fn main() -> glib::ExitCode {
    gio::resources_register_include!("org.dysliked.deste.gresource").expect("Failed to register resources");
    let application = Application::new();

    application.run()
}
