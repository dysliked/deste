/*
 * This file is part of Deste.
 *
 * Deste is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * Deste is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Deste. If
 * not, see <https://www.gnu.org/licenses/>.
*/

use diesel::prelude::*;

use crate::database;

use crate::schema::collection;

#[derive(Debug, Identifiable, Queryable)]
#[diesel(table_name = collection)]
pub struct DbCategory {
    id: i32,
    name: String,
    title: String,
    icon: String,
}

impl DbCategory {
    pub fn icon(&self) -> &String {
        &self.icon
    }

    pub fn id(&self) -> i32 {
        self.id
    }

    pub fn name(&self) -> &String {
        &self.name
    }

    pub fn title(&self) -> &String {
        &self.title
    }
}

pub fn get_all_categories() -> Vec<DbCategory> {
    let pool = database::get_pool();
    let connection = &mut pool.get().unwrap();

    collection::table.load(connection).unwrap()
}

pub fn get_category_from_name(category: &String) -> Option<DbCategory> {
    let pool = database::get_pool();
    let connection = &mut pool.get().unwrap();

    collection::table
        .filter(collection::name.eq(category))
        .load(connection)
        .unwrap()
        .into_iter()
        .nth(0)
}
