/*
 * This file is part of Deste.
 *
 * Deste is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * Deste is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Deste. If
 * not, see <https://www.gnu.org/licenses/>.
*/

use diesel::prelude::*;

use crate::database;
use crate::schema::collection;
use crate::schema::tree;

use crate::models::get_category_from_name;

#[derive(Debug,Insertable)]
#[diesel(table_name = tree)]
pub struct DbNewSource {
    type_id: i32,
    title: String,
    path: String,
}

#[derive(Debug, Identifiable, Queryable)]
#[diesel(table_name = tree)]
pub struct DbSource {
    id: i32,
    type_id: i32,
    title: String,
    path: String,
}

impl DbNewSource {
    pub fn new(type_id: i32, title: String, path: String) -> Self {
        Self{
            type_id: type_id,
            title: title,
            path: path,
        }
    }
}

impl DbSource {
    pub fn path(&self) -> &String {
        &self.path
    }

    pub fn title(&self) -> &String {
        &self.title
    }
}

pub fn add_source(category: &String, title: String, path: String) {
    let pool = database::get_pool();
    let connection = &mut pool.get().unwrap();

    if let Some(category) = get_category_from_name(category) {
        let tmp = diesel::insert_into(tree::table)
            .values(DbNewSource::new(category.id(), title, path))
            .execute(connection);

        println!("{:?}", tmp);
    }
}

pub fn get_sources(category: &String) -> Vec<DbSource> {
    let pool = database::get_pool();
    let connection = &mut pool.get().unwrap();

    tree::table
        .inner_join(collection::table)
        .filter(collection::name.eq(category))
        .select(tree::all_columns)
        .load(connection)
        .unwrap()
}

pub fn remove_source(title: &String) {
    let pool = database::get_pool();
    let connection = &mut pool.get().unwrap();

    let tmp = diesel::delete(tree::table)
        .filter(tree::title.eq(title))
        .execute(connection);

    println!("{:?}", tmp);
}
