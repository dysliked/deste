/*
 * This file is part of Deste.
 *
 * Deste is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * Deste is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Deste. If
 * not, see <https://www.gnu.org/licenses/>.
*/

use diesel::prelude::*;

use crate::database;

use crate::schema::*;

#[derive(Debug, Identifiable, Queryable)]
#[diesel(table_name = library)]
pub struct DbMedia {
    id: i32,
    path_id: i32,
    path: String,
    name: String,
    cover_path: String,
}

impl DbMedia {
    pub fn cover_path(&self) -> &String {
        &self.cover_path
    }

    pub fn name(&self) -> &String {
        &self.name
    }
}

pub fn get_medias(category: &String) -> Vec<DbMedia> {
    let pool = database::get_pool();
    let connection = &mut pool.get().unwrap();

    library::table
        .inner_join(tree::table.inner_join(collection::table))
        .filter(collection::name.eq(category))
        .select(library::all_columns)
        .load(connection)
        .unwrap()
}
