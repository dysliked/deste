/*
 * This file is part of Deste.
 *
 * Deste is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * Deste is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Deste. If
 * not, see <https://www.gnu.org/licenses/>.
*/

use std::cell::RefCell;

use gtk::glib;
use gtk::CompositeTemplate;

use adw::prelude::*;
use adw::subclass::prelude::*;

use crate::application::Application;
use crate::source::Source;

mod imp {
    use super::*;

    #[derive(Debug, Default, CompositeTemplate, glib::Properties)]
    #[properties(wrapper_type = super::SourceRow)]
    #[template(resource = "/org/dysliked/deste/ui/source_row.ui")]
    pub struct SourceRowImpl {
        #[property(get, set = Self::set_source, construct_only)]
        pub(super) source: RefCell<Option<Source>>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for SourceRowImpl {
        const NAME: &'static str = "DesteSourceRow";
        type Type = super::SourceRow;
        type ParentType = adw::ActionRow;

        fn class_init(klass: &mut Self::Class) {
            Self::bind_template(klass);
            Self::bind_template_callbacks(klass);
        }

        fn instance_init(object: &glib::subclass::InitializingObject<Self>) {
            object.init_template();
        }
    }

    #[glib::derived_properties]
    impl ObjectImpl for SourceRowImpl {
    }

    impl WidgetImpl for SourceRowImpl {
    }

    impl ListBoxRowImpl for SourceRowImpl {
    }

    impl PreferencesRowImpl for SourceRowImpl {
    }

    impl ActionRowImpl for SourceRowImpl {
    }

    #[gtk::template_callbacks]
    impl SourceRowImpl {
        #[template_callback]
        pub fn delete_source(&self) {
            let application = Application::default();

            application.remove_source(self.source.clone().into_inner().unwrap());
        }

        fn set_source(&self, source: Option<Source>) {
            let obj = self.obj();

            match &source {
                None => obj.set_title("<None>"),
                Some(src_obj) => {
                    obj.set_title(&src_obj.title());
                    obj.set_subtitle(&src_obj.path());
                },
            };

            self.source.replace(source);
        }
    }
}

glib::wrapper! {
    pub struct SourceRow(ObjectSubclass<imp::SourceRowImpl>)
        @extends gtk::Widget, gtk::ListBoxRow, adw::PreferencesRow, adw::ActionRow,
        @implements gtk::Buildable, gtk::Native;
}

impl SourceRow {
    pub fn new(source: Source) -> Self {
        glib::Object::builder()
            .property("source", source)
            .build()
    }
}
