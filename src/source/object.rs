/*
 * This file is part of Deste.
 *
 * Deste is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * Deste is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Deste. If
 * not, see <https://www.gnu.org/licenses/>.
*/

use std::cell::RefCell;

use gtk::glib;

use gtk::prelude::*;
use gtk::subclass::prelude::*;

mod imp {
    use super::*;

    #[derive(Debug, Default, glib::Properties)]
    #[properties(wrapper_type = super::Source)]
    pub struct SourceImpl {
        #[property(get, construct_only)]
        pub(super) path: RefCell<String>,
        #[property(get, construct_only)]
        pub(super) title: RefCell<String>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for SourceImpl {
        const NAME: &'static str = "DesteSource";
        type Type = super::Source;
    }

    #[glib::derived_properties]
    impl ObjectImpl for SourceImpl {
    }
}

glib::wrapper! {
    pub struct Source(ObjectSubclass<imp::SourceImpl>)
        ;
}

impl Source {
    pub fn new(path: &String, title: &String) -> Self {
        glib::Object::builder()
            .property("path", path)
            .property("title", title)
            .build()
    }
}

