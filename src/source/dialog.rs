/*
 * This file is part of Deste.
 *
 * Deste is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either
 *
 * version 3 of the License, or (at your option) any later version.
 *
 * Deste is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Deste. If
 * not, see <https://www.gnu.org/licenses/>.
*/

use std::cell::RefCell;

use gtk::glib;
use gtk::gio;

use gtk::CompositeTemplate;

use adw::prelude::*;
use adw::subclass::prelude::*;

use crate::application::Application;
use crate::category::Category;
use crate::source::Source;

mod imp {
    use super::*;

    #[derive(Debug, Default, CompositeTemplate, glib::Properties)]
    #[properties(wrapper_type = super::SourceDialog)]
    #[template(resource = "/org/dysliked/deste/ui/source_dialog.ui")]
    pub struct SourceDialogImpl {
        #[property(get, set = Self::set_category, construct_only)]
        pub(super) category: RefCell<Option<Category>>,
        #[property(get, set = Self::set_path, construct_only)]
        pub(super) path: RefCell<Option<gio::File>>,

        #[template_child]
        pub(super) name_entry: TemplateChild<adw::EntryRow>,
        #[template_child]
        pub(super) path_entry: TemplateChild<adw::ActionRow>,
        #[template_child]
        pub(super) type_entry: TemplateChild<adw::ActionRow>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for SourceDialogImpl {
        const NAME: &'static str = "DesteSourceDialog";
        type Type = super::SourceDialog;
        type ParentType = adw::Dialog;

        fn class_init(klass: &mut Self::Class) {
            Self::bind_template(klass);
            Self::bind_template_callbacks(klass);
        }

        fn instance_init(object: &glib::subclass::InitializingObject<Self>) {
            object.init_template();
        }
    }

    #[glib::derived_properties]
    impl ObjectImpl for SourceDialogImpl {
    }

    impl WidgetImpl for SourceDialogImpl {
    }

    impl AdwDialogImpl for SourceDialogImpl {
    }

    #[gtk::template_callbacks]
    impl SourceDialogImpl {
        #[template_callback]
        pub fn add(&self) {
            let source = Source::new(&self.path_entry.subtitle().unwrap().to_string(), &self.name_entry.text().to_string());
            let application = Application::default();

            application.add_source(&self.category.clone().into_inner().unwrap(), source);
            self.obj().force_close();
        }

        #[template_callback]
        pub fn cancel(&self) {
            self.obj().force_close();
        }

        pub fn set_category(&self, category: Option<Category>) {
            match &category {
                None => self.type_entry.set_subtitle(""),
                Some(x) => self.type_entry.set_subtitle(&x.title()),
            };

            self.category.replace(category);
        }

        pub fn set_path(&self, path: Option<gio::File>) {
            match &path {
                None => self.path_entry.set_subtitle(""),
                Some(x) => self.path_entry.set_subtitle(&x.uri()),
            }

            self.path.replace(path);
        }
    }
}

glib::wrapper! {
    pub struct SourceDialog(ObjectSubclass<imp::SourceDialogImpl>)
        @extends gtk::Widget, adw::Dialog,
        @implements gtk::Native;
}

impl SourceDialog {
    pub fn new(category: &Category, path: &gio::File) -> Self {
        glib::Object::builder()
            .property("category", category)
            .property("path", path)
            .build()
    }
}
