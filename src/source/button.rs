/*
 * This file is part of Deste.
 *
 * Deste is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either
 *
 * version 3 of the License, or (at your option) any later version.
 *
 * Deste is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Deste. If
 * not, see <https://www.gnu.org/licenses/>.
*/

use std::cell::RefCell;

use gtk::glib;
use gtk::gio;

use adw::prelude::*;
use adw::subclass::prelude::*;

use crate::category::Category;
use crate::source::SourceDialog;

mod imp {
    use super::*;

    #[derive(Debug, Default, gtk::CompositeTemplate, glib::Properties)]
    #[properties(wrapper_type = super::NewSourceButton)]
    #[template(resource = "/org/dysliked/deste/ui/new_source_button.ui")]
    pub struct NewSourceButtonImpl {
        #[property(get, set, construct_only)]
        pub(super) category: RefCell<Option<Category>>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for NewSourceButtonImpl {
        const NAME: &'static str = "DesteNewSourceButton";
        type Type = super::NewSourceButton;
        type ParentType = gtk::Button;

        fn class_init(klass: &mut Self::Class) {
            Self::bind_template(klass);
            Self::bind_template_callbacks(klass);
        }

        fn instance_init(object: &glib::subclass::InitializingObject<Self>) {
            object.init_template();
        }
    }

    #[glib::derived_properties]
    impl ObjectImpl for NewSourceButtonImpl {
    }

    impl WidgetImpl for NewSourceButtonImpl {
    }

    impl ButtonImpl for NewSourceButtonImpl {
    }

    #[gtk::template_callbacks]
    impl NewSourceButtonImpl {
        #[template_callback]
        pub fn create_source(&self) {
            let obj = self.obj();
            let parent = obj.root().unwrap().downcast::<gtk::Window>().unwrap();
            let path_dialog = gtk::FileDialog::new();

            path_dialog.select_folder(Some(&parent), gio::Cancellable::NONE, glib::clone!(#[weak] obj, #[weak] parent, move |result| {
                match result {
                    Ok(path) => SourceDialog::new(&obj.category().unwrap(), &path).present(Some(&parent)),
                    _ => {},
                }
            }));
        }
    }
}

glib::wrapper! {
    pub struct NewSourceButton(ObjectSubclass<imp::NewSourceButtonImpl>)
        @extends gtk::Widget, gtk::Button,
        @implements gtk::Native;
}

impl NewSourceButton {
    pub fn new(category: &Category) -> Self {
        glib::Object::builder()
            .property("category", category)
            .build()
    }
}
