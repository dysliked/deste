-- Your SQL goes here
CREATE TABLE collection (
	id INTEGER NOT NULL,
	name TEXT UNIQUE NOT NULL,
	title TEXT NOT NULL,
	icon TEXT NOT NULL,
	PRIMARY KEY (id)
);

CREATE TABLE tree (
	id INTEGER NOT NULL,
	type_id INTEGER NOT NULL,
	title TEXT UNIQUE NOT NULL,
	path TEXT NOT NULL,
	PRIMARY KEY (id),
	FOREIGN KEY (type_id) REFERENCES collection(id)
);

CREATE TABLE library (
	id INTEGER NOT NULL,
	path_id INTEGER NOT NULL,
	path TEXT NOT NULL,
	name TEXT NOT NULL,
	cover_path TEXT NOT NULL,
	PRIMARY KEY (id),
	FOREIGN KEY (path_id) REFERENCES tree(id)
)
