-- Your SQL goes here

INSERT INTO collection (name, title, icon) VALUES ("movies", "Movies", "video-x-generic-symbolic");
INSERT INTO collection (name, title, icon) VALUES ("tv-shows", "TV Shows", "folder-symbolic");
INSERT INTO collection (name, title, icon) VALUES ("music", "Music", "audio-x-generic-symbolic");

INSERT INTO tree (type_id, title, path) VALUES (3, "User's Music", "file:///home/tpeypelu/Music");
INSERT INTO tree (type_id, title, path) VALUES (1, "User's Movies", "file:///home/tpeypelu/Videos/Movies");
INSERT INTO tree (type_id, title, path) VALUES (2, "User's TV Shows", "file:///home/tpeypelu/Videos/TV Shows");

---INSERT INTO library (path_id, path, name) VALUES (1, "Mark Korven/The Peripheral/01-01-Mark_Korven-Main_Title-SMR.flac", "Main Title");
INSERT INTO library (path_id, path, name, cover_path) VALUES (2, "Amsterdam (2022).mkv", "Amsterdam", "/home/tpeypelu/Downloads/Amsterdam.jpg");
INSERT INTO library (path_id, path, name, cover_path) VALUES (2, "Anna (2019).mkv", "Anna", "/home/tpeypelu/Downloads/Anna.jpg");
